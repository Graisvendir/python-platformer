from pathlib import Path

from src.constants import PLATFORM_WEIGHT, PLATFORM_HEIGHT


class Level:

    def __init__(self, file_path: Path):
        self.file_path = file_path

        self.platforms = []
        self.coins = []
        self.traps = []
        self.player = ()

        file = file_path.read_text()
        lines = file.split('\n')

        self.height = len(lines) * PLATFORM_HEIGHT
        self.width = len(lines[0]) * PLATFORM_WEIGHT

        for i in range(len(lines)):
            for j in range(len(lines[i])):
                if lines[i][j] == '=':
                    self.platforms.append((j * PLATFORM_WEIGHT, i * PLATFORM_HEIGHT, 'hor'))
                elif lines[i][j] == '|':
                    self.platforms.append((j * PLATFORM_WEIGHT, i * PLATFORM_HEIGHT, 'vert'))
                elif lines[i][j] == '@':
                    self.player = (j * PLATFORM_WEIGHT, i * PLATFORM_HEIGHT)
                elif lines[i][j] == '*':
                    self.coins.append((j * PLATFORM_WEIGHT, i * PLATFORM_HEIGHT))
                elif lines[i][j] == 't':
                    self.traps.append((j * PLATFORM_WEIGHT, i * PLATFORM_HEIGHT))
