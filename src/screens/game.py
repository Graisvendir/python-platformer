import pygame
from pathlib import Path

import src.constants as constants
from src.ui import UI
from src.sprite.player import Player
from src.sprite.coin import Coin
from src.sprite.platform import Platform
from src.level import Level
from src.screens.screen import Screen
from src.sprite.trap import Trap
from src.sound import win_sound


class Game(Screen):
    def __init__(self, screen):
        super().__init__(screen)
        self.bg = pygame.image.load(constants.BG_IMG)

        self.levels = []
        self.level = 0

        self.init_levels()
        self.set_level(0)

    def init_levels(self):
        levels_path = Path(str(Path(__file__).parent.parent.parent) + '/levels')

        for file in levels_path.iterdir():
            self.levels.append(Level(file))

    def set_level(self, num: int):
        if num in range(len(self.levels)):
            self.level = num
            level = self.levels[self.level]
            self.screen = pygame.display.set_mode((level.width, level.height))
            self.init_sprites(level)

    def restart(self):
        self.set_level(0)

    def init_sprites(self, level: Level):
        self.ui = UI(self.screen)

        self.platforms = pygame.sprite.Group()
        for platform in level.platforms:
            self.platforms.add(Platform(platform[0], platform[1], constants.PLATFORM_WEIGHT, constants.PLATFORM_HEIGHT, platform[2]))

        self.traps = pygame.sprite.Group()
        for trap in level.traps:
            self.traps.add(
                Trap(trap[0], trap[1], constants.PLATFORM_WEIGHT, constants.PLATFORM_HEIGHT))

        player = Player(level.player[0], level.player[1], self.platforms, self.traps)
        self.units = pygame.sprite.Group()
        self.units.add(player)

        self.coins = pygame.sprite.Group()
        for coin in level.coins:
            self.coins.add(Coin(coin[0], coin[1], self.platforms, player))

    def screen_update(self):
        self.update_level()
        self.coins.update()
        self.units.update()
        self.platforms.update()
        self.traps.update()

    def update_level(self):
        if not self.coins:
            if self.level >= len(self.levels) - 1:
                win_sound()
                pygame.event.post(pygame.event.Event(constants.WIN_GAME))
            else:
                self.set_level(self.level + 1)

    def render(self):
        self.screen.blit(self.bg, (0, 0))
        self.coins.draw(self.screen)
        self.units.draw(self.screen)
        self.platforms.draw(self.screen)
        self.traps.draw(self.screen)
        self.ui.update()

        # показываем новый экран
        pygame.display.flip()
