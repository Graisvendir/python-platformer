
class Screen:
    def __init__(self, screen):
        self.screen = screen

    def event_handlers(self):
        """
        1
        Собирает все события и обрабатывает их
        :return:
        """
        pass

    def screen_update(self):
        """
        2
        Обновление всех компонентов игры
        :return:
        """
        pass

    def render(self):
        """
        3
        Прорисовка игры и "переворачивание экрана"
        :return:
        """
        pass