import pygame
import pygame_menu

from src.screens.screen import Screen
from src.constants import START_GAME


class Menu(Screen):
    def __init__(self, screen):
        super().__init__(screen)

        self.menu = pygame_menu.Menu('Главное меню', 300, 400, True,
                                     theme=pygame_menu.themes.THEME_BLUE)

        self.menu.add.button('Играть', self.start_game)
        self.menu.add.button('Выйти', pygame_menu.events.EXIT)
        self.menu.center_content()

    def start_game(self):
        self.menu.disable()
        pygame.event.post(pygame.event.Event(START_GAME))

    def enable(self):
        self.menu.enable()

    def render(self):
        """
        3
        Прорисовка игры и "переворачивание экрана"
        :return:
        """
        self.menu.mainloop(self.screen)
