import pygame
import src.constants as constants

from src.sprite.animated import AnimatedSprite
from src.sprite.player import Player


class Coin(AnimatedSprite):
    def __init__(self, x: int, y: int, platforms: pygame.sprite.Group, player: Player):
        super().__init__(constants.COIN_IMG, 8, 1, x, y)
        self.platforms = platforms
        self.player = player

    def update(self):
        super().update()

        if not pygame.sprite.spritecollideany(self, self.platforms):
            self.rect.y += constants.GRAVITY

        if pygame.sprite.collide_rect(self, self.player):
            pygame.event.post(pygame.event.Event(constants.PLAYER_CATCH_COIN))
            self.kill()
