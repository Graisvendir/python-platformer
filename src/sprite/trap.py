import pygame
from src.constants import TRAP


class Trap(pygame.sprite.Sprite):
    def __init__(self, x: int, y: int, width: int, height: int):
        super().__init__()

        self.image = pygame.image.load(TRAP)
        self.image.set_colorkey((89, 145, 194))
        self.rect = pygame.Rect(x, y, width, height)
