import pygame
import src.constants as constants

from src.sprite.animated import AnimatedSprite
from src.sound import loose_sound


class Player(AnimatedSprite):
    def __init__(self, x: int, y: int, platforms: pygame.sprite.Group, traps: pygame.sprite.Group):
        super().__init__(constants.PLAYER_STAND_IMG, 4, 1, x, y)
        self.platforms = platforms
        self.traps = traps

        self.speed_x = 0
        self.speed_y = 0
        self.on_ground = True

        self.rect = self.image.get_rect()
        self.rect.center = (x, y)

    def update(self):
        super().update()
        self.image.set_colorkey((89, 145, 194))

        keystate = pygame.key.get_pressed()
        left = keystate[pygame.K_LEFT]
        right = keystate[pygame.K_RIGHT]
        jump = keystate[pygame.K_UP]

        if left:
            self.speed_x = -constants.MOVE_SPEED
            self.image = pygame.transform.flip(self.image, True, False)
        if right:
            self.speed_x = +constants.MOVE_SPEED
        if not left and not right:
            self.speed_x = 0
        if jump and self.on_ground:
            self.speed_y = -constants.JUMP_SPEED

        if not self.on_ground:
            self.speed_y += constants.GRAVITY

        self.on_ground = False

        self.rect.y += self.speed_y
        self.collide(0, self.speed_y)

        self.rect.x += self.speed_x
        self.collide(self.speed_x, 0)

    def collide(self, speed_x: int, speed_y: int):
        for trap in self.traps:
            if pygame.sprite.collide_rect(self, trap):  # если есть пересечение платформы с игроком
                loose_sound()
                pygame.event.post(pygame.event.Event(constants.LOOSE_GAME))

        for p in self.platforms:
            if pygame.sprite.collide_rect(self, p):  # если есть пересечение платформы с игроком

                if speed_x > 0:                      # если движется вправо
                    self.rect.right = p.rect.left  # то не движется вправо

                if speed_x < 0:                      # если движется влево
                    self.rect.left = p.rect.right  # то не движется влево

                if speed_y > 0:                      # если падает вниз
                    self.rect.bottom = p.rect.top  # то не падает вниз
                    self.on_ground = True            # и становится на что-то твердое
                    self.speed_y = 0                 # и энергия падения пропадает

                if speed_y < 0:                      # если движется вверх
                    self.rect.top = p.rect.bottom  # то не движется вверх
                    self.speed_y = 0                 # и энергия прыжка пропадает
