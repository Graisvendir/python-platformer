import pygame
from src.constants import GROUND_IMG, GROUND_VERT_IMG


class Platform(pygame.sprite.Sprite):
    def __init__(self, x: int, y: int, width: int, height: int, type: str = 'hor'):
        """

        :param x: int
        :param y: int
        :param width: int
        :param height: int
        :param type: str    Тип платформы (горизонтальная/вертикальная)
        """
        super().__init__()

        if type == 'hor':
            img = GROUND_IMG
        else:
            img = GROUND_VERT_IMG
        self.image = pygame.image.load(img)
        self.rect = pygame.Rect(x, y, width, height)
