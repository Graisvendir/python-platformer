import pygame
from pathlib import Path

from src.constants import SPRITE_ANIMATION_SPEED


class AnimatedSprite(pygame.sprite.Sprite):
    def __init__(self, image_path: Path, columns, rows, x=0, y=0):
        pygame.sprite.Sprite.__init__(self)

        self.sprite_frame = 0

        image = pygame.image.load(image_path)
        self.rect = pygame.Rect(0, 0, image.get_width() // columns, image.get_height() // rows)
        self.frames = self.cut_image(image, columns, rows)

        self.cur_frame = 0
        self.image = self.frames[self.cur_frame]
        self.rect = self.rect.move(x, y)

    def cut_image(self, image: pygame.Surface, columns: int, rows: int):
        frames = []

        for j in range(rows):
            for i in range(columns):
                frame = image.subsurface(pygame.Rect((self.rect.w * i, self.rect.h * j), self.rect.size))
                frames.append(frame)

        return frames

    def update(self):
        # искусственно замедляем анимацию спрайта
        self.sprite_frame += 1
        if self.sprite_frame == SPRITE_ANIMATION_SPEED:
            self.cur_frame = (self.cur_frame + 1) % len(self.frames)
            self.sprite_frame = 0

        self.image = self.frames[self.cur_frame]
