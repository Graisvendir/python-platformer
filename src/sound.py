import pygame

from src.constants import WIN_SOUND, COIN_SOUND, LOOSE_SOUND


def coin_sound():
    pygame.mixer.music.load(COIN_SOUND)
    pygame.mixer.music.play(0)


def win_sound():
    pygame.mixer.music.load(WIN_SOUND)
    pygame.mixer.music.play(0)


def loose_sound():
    pygame.mixer.music.load(LOOSE_SOUND)
    pygame.mixer.music.play(0)
