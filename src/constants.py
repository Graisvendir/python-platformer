from pathlib import Path
import pygame

PLAYER_CATCH_COIN = pygame.USEREVENT + 1
START_GAME = pygame.USEREVENT + 2
MAIN_MENU = pygame.USEREVENT + 3
WIN_GAME = pygame.USEREVENT + 4
LOOSE_GAME = pygame.USEREVENT + 5

WIDTH = 800     # ширина игрового окна
HEIGHT = 650    # высота игрового окна
FPS = 60        # частота кадров в секунду

SPRITE_ANIMATION_SPEED = 8  # Как часто будет меняться изображение спрайта

# настройки физики персонажа и двигаемых сущностей на уровне
GRAVITY = 5
JUMP_SPEED = 40
MOVE_SPEED = 7

# настройки платформ
PLATFORM_HEIGHT = 32
PLATFORM_WEIGHT = 32

# цвета
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

path = Path(__file__)
GAME_DIR = path.parent

# Картинки
IMG_DIR = Path(str(GAME_DIR.parent) + '/img')

BG_IMG = Path(str(IMG_DIR) + '/bg.jpg')
COIN_IMG = Path(str(IMG_DIR) + '/coin.png')
PLAYER_STAND_IMG = Path(str(IMG_DIR) + '/player/stand.png')
GROUND_IMG = Path(str(IMG_DIR) + '/ground.jpg')
GROUND_VERT_IMG = Path(str(IMG_DIR) + '/ground-vert.jpg')
TRAP = Path(str(IMG_DIR) + '/trap.png')

# Уровни
LEVELS_DIR = Path(str(GAME_DIR.parent) + '/levels')

# Звуки
SOUND_DIR = Path(str(GAME_DIR.parent) + '/sound')

COIN_SOUND = Path(str(SOUND_DIR) + '/coin.mp3')
LOOSE_SOUND = Path(str(SOUND_DIR) + '/loose.mp3')
WIN_SOUND = Path(str(SOUND_DIR) + '/win.mp3')