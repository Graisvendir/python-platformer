import pygame

from src.constants import WHITE, PLAYER_CATCH_COIN
from src.sound import coin_sound


class UI:
    def __init__(self, screen: pygame.Surface):
        self.coin_catched = 0
        pygame.font.init()
        self.font = pygame.font.SysFont('arial', 24)
        self.screen = screen

    def update(self):
        if pygame.event.peek(PLAYER_CATCH_COIN):
            self.coin_catched += 1
            coin_sound()

        surface = self.font.render('Coins: ' + str(self.coin_catched), True, WHITE)
        text = surface.get_rect(x=10, y=10)
        self.screen.blit(surface, text)
