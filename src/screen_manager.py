import pygame

import src.constants as constants
from src.screens.menu import Menu
from src.screens.game import Game
from src.screens.win import Win


class ScreenManager:
    """
    Главный сервис игры. Управляет экранами
    """

    def __init__(self):
        pygame.init()
        pygame.mixer.init()  # для звука
        pygame.display.set_caption("Платформер")

        self.screen = pygame.display.set_mode((constants.WIDTH, constants.HEIGHT))
        self.clock = pygame.time.Clock()

        self.screen_list = {
            'game': Game(self.screen),
            'menu': Menu(self.screen),
            'win': Win(self.screen),
        }
        self.cur_screen = self.screen_list['menu']
        self.running = True

    def start(self):
        """
        Основной поток игры
        :return:
        """

        while self.running:
            self.clock.tick(constants.FPS)

            self.event_handlers()

            # события
            self.cur_screen.event_handlers()

            # обновление экрана
            self.cur_screen.screen_update()

            # рендеринг
            self.cur_screen.render()

    def event_handlers(self):
        """
        Глобальные события обрабатывать здесь
        В том числе переходы между экранами
        :return:
        """
        keystate = pygame.key.get_pressed()
        escape = keystate[pygame.K_ESCAPE]
        if escape:
            self.cur_screen = self.screen_list['menu']
            self.cur_screen.enable()

        for event in pygame.event.get():
            # проверить закрытие окна
            if event.type == pygame.QUIT:
                self.running = False
            elif event.type == constants.START_GAME:
                self.cur_screen = self.screen_list['game']
                self.cur_screen.restart()
            elif event.type == constants.WIN_GAME:
                self.cur_screen = self.screen_list['win']
                self.cur_screen.enable()
            elif event.type == constants.LOOSE_GAME:
                self.cur_screen.restart()
